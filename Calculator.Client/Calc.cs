﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Calculator.Client
{
  public class Calc
  {
    public string ExecuteExpression(string op)
    {
      string numbersTemplate = @"\d{1,}";
      string symbolTemplate = @"(\*|\/|\\|\+|-)";

      var symbolMatch = Regex.Match(op, symbolTemplate);
      var numbersMatch = Regex.Match(op, numbersTemplate);

      string operation = "";
      int a = 0, b = 0; 

      if (symbolMatch.Success)
      {
        operation = symbolMatch.Groups[0].Captures[0].ToString();
      }
      else
      {
        return "Wrong operation";
      }
      if (numbersMatch.Success)
      {
        a = Convert.ToInt32(numbersMatch.Value);
        numbersMatch = numbersMatch.NextMatch();
        if(numbersMatch.Success)
          b = Convert.ToInt32(numbersMatch.Value);
        else
        {
          return "Wrong operation";
        }
      }
      else
      {
        return "Wrong operation";
      }

      switch (operation)
      {
        case "+":
          return Add(a, b).ToString();
        case "-":
          return Subtract(a, b).ToString();
        case "*":
          return Multiply(a, b).ToString();
        case "/":
        case "\\":
          if (b == 0)
            return "You can not divide by 0!";
          return Divide(a, b).ToString();
        default:
          return String.Format("Something went wrong: {0} {1} {2}", a, operation, b);
      }
    }
    public int Add(int a, int b)
    {
      return a + b;
    }

    public int Subtract(int a, int b)
    {
      return a - b;
    }

    public int Multiply(int a, int b)
    {
      return a * b;
    }

    public float Divide(int a, int b)
    {
      if (b == 0)
      {
        return 0;
      }
      else
      {
        return (float)a / (float)b;
      }
    }
  }
}
