﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Client
{
  class Program
  {
    static void Main(string[] args)
    {
      var calc = new Calc();
      string expr = "";

      while (true)
      {
        Console.Write("Please enter operation '+|-|*|/' between two numbers (ex. 10+20)\n or type 'exit' or 'quit' to exit application: ");
        expr = Console.ReadLine().ToLower();
        if(expr == "exit" || expr == "quit") {
          Console.WriteLine("Good bye!");
          break;
        } else {
          Console.WriteLine(calc.ExecuteExpression(expr) + "\n\n\n");
        }
      }
    }
  }
}
