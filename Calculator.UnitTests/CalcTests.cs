﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calculator.Client;

namespace Calculator.UnitTests
{
  [TestClass]
  public class CalcTests
  {
    [TestCategory("Basic operations")]
    [TestMethod]
    public void Should_Return_Proper_Addition_Result()
    {
      //Arrange
      var calc = new Calc();
      int result = 0;

      //Act
      result = calc.Add(1, 2);

      //Assert
      Assert.AreEqual(3, result, "Numbers '1' and '2' are added properly.");
    }
    [TestMethod]
    public void Should_Return_Proper_Subtract_Result()
    {
      //Arrange
      var calc = new Calc();
      int result = 0;

      //Act
      result = calc.Subtract(10, 20);

      //Assert
      Assert.AreEqual(-10, result, "20 is properly subtracted from 10.");
    }
    [TestMethod]
    public void Should_Return_Proper_Multiplication_Result()
    {
      //Arrange
      var calc = new Calc();
      int result = 0;

      //Act
      result = calc.Multiply(-11, 11);

      //Assert
      Assert.AreEqual(-121, result, "Numbers '-11' and '11' are multiplied properly.");
    }
    [TestMethod]
    public void Should_Return_Proper_Division_Result()
    {
      //Arrange
      var calc = new Calc();
      float result = 0;

      //Act
      result = calc.Divide(5, 2);

      //Assert
      Assert.AreEqual(2.5, result, "5 is properly divided by 2.");
    }
    [TestMethod]
    public void Should_not_be_able_to_divide_by_zero()
    {
      //Arrange
      var calc = new Calc();
      float result = 0;

      //Act
      result = calc.Divide(20, 0);

      //Assert
      Assert.AreEqual(0, result, "Result of division by 0 is 0. Operation is handled properly");
    }
    [TestCategory("Operation parser")]
    [TestMethod]
    public void Should_properly_parse_addition_operation_string()
    {
      //Arrange
      var calc = new Calc();
      string result = "";

      //Act
      result = calc.ExecuteExpression("20+10");

      //Assert
      Assert.AreEqual("30", result, "'20+10' addition string parsed properly.");
    }
    [TestMethod]
    public void Should_properly_parse_subtraction_operation_string()
    {
      //Arrange
      var calc = new Calc();
      string result = "";

      //Act
      result = calc.ExecuteExpression("100-50");

      //Assert
      Assert.AreEqual("50", result, "'100-50' subtraction string parsed properly.");
    }
    [TestMethod]
    public void Should_properly_parse_multiplication_operation_string()
    {
      //Arrange
      var calc = new Calc();
      string result = "";

      //Act
      result = calc.ExecuteExpression("20*10");

      //Assert
      Assert.AreEqual("200", result, "'20*10' multiplication string parsed properly.");
    }
    [TestMethod]
    public void Should_properly_parse_division_operation_string()
    {
      //Arrange
      var calc = new Calc();
      string result = "";

      //Act
      result = calc.ExecuteExpression("20/10");

      //Assert
      Assert.AreEqual("2", result, "'20/10' division string parsed properly.");
    }
    [TestMethod]
    public void Should_properly_parse_and_handle_division_by_zero()
    {
      //Arrange
      var calc = new Calc();
      string result = "";

      //Act
      result = calc.ExecuteExpression("100/0");

      //Assert
      Assert.AreEqual("You can not divide by 0!", result, "Proper message put, if user tries to divide by 0.");
    }
    [TestMethod]
    public void Spaces_in_string_expression_does_not_matter()
    {
      //Arrange
      var calc = new Calc();
      string result = "";

      //Act
      result = calc.ExecuteExpression(" 100 + 200 ");

      //Assert
      Assert.AreEqual("300", result, "Result is proper even if additional spaces are available in expression.");
    }
  }
}
